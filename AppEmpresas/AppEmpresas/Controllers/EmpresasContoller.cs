﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using AppEmpresas.Models;
using AppEmpresas.Services;

namespace AppEmpresas.Controllers
{
    [Authorize]
    public class EmpresasContoller : Controller
    {
        private readonly IEmpresaService _empresaService;
        private readonly UserManager<IdentityUser> _userManager;

        public EmpresasContoller(IEmpresaService empresaService,
            UserManager<IdentityUser> userManager)
        {
            _empresaService = empresaService;
            _userManager = userManager;
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index()
        {
            var currentUser = await _userManager.GetUserAsync(User);

            if (currentUser == null) return Challenge();

            var empresas = await _empresaService
                      .GetEmpresasAsync(currentUser);

            var model = new EmpresaViewModel()
            {
                Empresas = empresas
            };

            return View(model);
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEmpresa(Empresa novaEmpresa)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            var currentUser = await _userManager.GetUserAsync(User);

            if (currentUser == null) return Challenge();

            var successful = await _empresaService
                      .AddEmpresaAsync(novaEmpresa, currentUser);

            if (!successful)
            {
                return BadRequest("Não foi possível adicionar o item.");
            }
            return RedirectToAction("Index");
        }
    }
}
