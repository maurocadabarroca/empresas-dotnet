﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using AppEmpresas.Models;

namespace AppEmpresas.Data
{
  public class ApplicationDbContext : DbContext
  {
    public ApplicationDbContext(
        DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<Empresa> Empresas { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);
    }
  }
}
