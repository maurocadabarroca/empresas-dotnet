﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using AppEmpresas.Models;

namespace AppEmpresas.Services
{
  public interface IEmpresaService
  {
    Task<Empresa[]> GetEmpresasAsync(IdentityUser user);

    Task<bool> AddEmpresaAsync(Empresa novaEmpresa, IdentityUser user);
  }
}
