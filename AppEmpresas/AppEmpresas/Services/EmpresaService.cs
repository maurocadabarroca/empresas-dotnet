﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;
using AppEmpresas.Models;
using Microsoft.EntityFrameworkCore;
using AppEmpresas.Data;

namespace AppEmpresas.Services
{
  public class EmpresaService : IEmpresaService
  {
    private readonly ApplicationDbContext _context;

    public EmpresaService(ApplicationDbContext context)
    {
      _context = context;
    }

    public async Task<bool> AddEmpresaAsync(Empresa novaEmpresa, IdentityUser user)
    {
      novaEmpresa.Id = Guid.NewGuid();
      _context.Empresas.Add(novaEmpresa);
      novaEmpresa.UserId = user.Id;

      var saveResult = await _context.SaveChangesAsync();
      return saveResult == 1;
    }

    public async Task<Empresa[]> GetEmpresasAsync(IdentityUser user)
    {
            //hardcode
      return await _context.Empresas
              .Where(e => e.Name.Contains("sis") && e.UserId == user.Id)
              .ToArrayAsync();
    }
  }
}
