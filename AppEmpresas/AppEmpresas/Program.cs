﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace AppEmpresas
{
    // Usando dotnet --version 2.2.107
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args)
                .Build()
                .Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                    .ConfigureKestrel((context, options) =>
                    {
                        options.AddServerHeader = false;
                    });

        private static void InitializeDatabase(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    SeedData.InitializeAsync(services).Wait();
                }
                catch (Exception ex)
                {
                    var logger = services
                    .GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "Ocorreu um erro inicializando (seeding) o DB.");
                }
            }
        }
    }
}
